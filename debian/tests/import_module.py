import sys
from django.core.exceptions import ImproperlyConfigured
try: 
    from test_without_migrations.management.commands._base import CommandMixin, TestCommand
except ImproperlyConfigured:
     sys.exit(0)
else:
    sys.exit(1)
